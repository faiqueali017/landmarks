//
//  LandmarkList.swift
//  Landmarks
//
//  Created by Faiq on 04/04/2021.
//

import SwiftUI

struct LandmarkList: View {
    @EnvironmentObject var modelData: ModelData
    @State private var showFavoritesOnly = false
    
    var filteredLandmarks: [Landmark] {
        ///Will only filter when show fav is true otherwise will pass same array
        modelData.landmarks.filter { landmark in
            (!showFavoritesOnly || landmark.isFavorite)
        }
    }
    
    var body: some View {
        
        //embbed list in navigation
        NavigationView {
            //pass landmark array
            List {
                //TOGGLE SECTION
                Toggle(isOn: $showFavoritesOnly) {
                    Text("Favorites only")
                }
                
                //LIST SECTION
                ForEach(filteredLandmarks) { landmark in
                    //Add navigation link
                    NavigationLink(destination: LandmarkDetail(landmark: landmark)) {
                        LandmarkRow(landmark: landmark)
                    }
                }
            }
            .navigationTitle("Landmarks")
        }
        
    }
}

struct LandmarkList_Previews: PreviewProvider {
    static var previews: some View {
        
        /// FOR MORE THAN ONE DEVICES PREVIEW
//        ForEach(["iPhone SE (2nd generation)", "iPhone 12 Pro Max"], id: \.self) { deviceName in
//            LandmarkList()
//                .previewDevice(PreviewDevice(rawValue: deviceName))
//                .previewDisplayName(deviceName)
//        }
//        .preferredColorScheme(.light)
        
        ///FOR DEFAULT DEVICE PREVIEW
        LandmarkList()
        
    }
}
